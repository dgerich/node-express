const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('/:id', function(req, res, next) {
    res.render('deleteuser', { title: 'Delete' });
    let a;
    const list_users = fs.readFile('./list-users.json', function(err, data) {
        if (err) throw err;
        a = JSON.parse(data);
        let i = a.findIndex((el)=>{
            if(el.id === req.params .id) return true;
        });a.splice(i, 1);
        let itm = JSON.stringify(a);
        fs.writeFile('list-users.json', itm , function(error){
            if(error) throw error;
        });
    });
});


module.exports = router;