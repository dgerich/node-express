const express = require('express');
const fs = require('fs');
const router = express.Router();

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });
//сделать черрез get
  router.get('/', function (req, res, next) {
    fs.readFile('./list-users.json', "utf-8", function (err, data) {
      if (err) throw err;
      let arr = JSON.parse(data);
      res.render('users', {title: "List Users", list: arr});
    });
  });

  router.post('/', function (req, res) {
    res.redirect('/users');
  });



module.exports = router;
module.exports = router;



