var express = require('express');
var router = express.Router();
const fs = require('fs');


router.route('/edituser')
{
    router.get('/:id', function (req, res, next) {
        let userId = req.params.id;
        let a;
        const list_users = fs.readFile('./list-users.json', function (err, data) {
            if (err) throw err;
            a = JSON.parse(data);
            let i = a.findIndex((el) => {
                if (el.id === req.params.id) return true;
            });
            res.render('edituser', {title: 'Edit User', itm: a[i]});
        })
    });

    router.post("/:id", function (req, res) {
        let userId = req.params.id;
        let a;
        const list_users = fs.readFile('./list-users.json', function (err, data) {
            if (err) throw err;
            a = JSON.parse(data);
            let i = a.findIndex((el) => {
                if (el.id === req.params.id) return true;
            });
            req.body.id = req.params.id;
            a.splice(i, 1, req.body);
            let itm = JSON.stringify(a);
            fs.writeFile('list-users.json', itm, function (error) {
                if (error) throw error;
            });
            res.render('edituser', {title: 'Edit User', itm: a[i]});
        })
    });
}
module.exports = router;

module.exports = router;